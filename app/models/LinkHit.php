<?php

namespace app\models;

use app\components\CustomerJourneyService;
use Yii;
use yii\db\ActiveQuery;
use yii\db\Exception;

/**
 * This is the model class for table "link_hit".
 *
 * @property int $id
 * @property string $url
 * @property string $link_type
 * @property int $hit_timestamp
 * @property string|null $customer_id
 *
 * @property Customer $customer
 */
class LinkHit extends \yii\db\ActiveRecord
{
    /**
     * @var CustomerJourneyService
     */
    protected $customerJourney;

    /**
     * LinkHit constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->customerJourney = new CustomerJourneyService();
    }

    /**
     * @param CustomerJourneyService $customerJourney
     */
    public function setCustomerJourney(CustomerJourneyService $customerJourney)
    {
        $this->customerJourney = $customerJourney;
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'link_hit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['url', 'link_type', 'hit_timestamp', 'customer_id'], 'required'],
            [['hit_timestamp'], 'integer'],
            [['url'], 'string', 'max' => 2000],
            [['link_type'], 'string', 'max' => 50],
            [['customer_id'], 'string', 'max' => 255],
            [['link_type'], 'in', 'range' => ['product', 'category', 'static-page', 'checkout', 'homepage']]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'url' => 'Url',
            'link_type' => 'Link Type',
            'hit_timestamp' => 'Hit Timestamp',
            'customer_id' => 'Customer ID',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getCustomer(): ActiveQuery
    {
        return $this->hasOne(Customer::class, ['id' => 'customer_id']);
    }

    /**
     * @param bool $runValidation
     * @param null $attributeNames
     * @return bool
     * @throws Exception
     */
    public function save($runValidation = true, $attributeNames = null): bool
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $parent = parent::save($runValidation, $attributeNames);
            $hash = $this->customerJourney->computeCustomerJourneyHash($this->customer_id);
            $customer = $this->customer ?? new Customer();
            $customer->id = $this->customer_id;
            $customer->journey = $hash;
            if ($parent && $customer->save()) {
                $transaction->commit();
                return true;
            }
        } catch (Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
        $transaction->rollBack();
        return false;
    }
}
