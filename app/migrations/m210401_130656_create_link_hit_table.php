<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%link_hit}}`.
 */
class m210401_130656_create_link_hit_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%link_hit}}', [
            'id' => $this->primaryKey(),
            'url' => $this->string(2000)->notNull(),
            'link_type' => $this->string(50)->notNull(),
            'hit_timestamp' => $this->integer(11)->notNull(),
            'customer_id' => $this->string(255)->notNull(),
        ]);
        $this->createIndex('url_idx', 'link_hit', 'url');
        $this->createIndex('link_type_idx', 'link_hit', 'link_type');
        $this->createIndex('hit_timestamp_idx', 'link_hit', 'hit_timestamp');
        $this->createIndex('customer_id_idx', 'link_hit', 'customer_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%link_hit}}');
    }
}
