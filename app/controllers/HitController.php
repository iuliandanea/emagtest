<?php

namespace app\controllers;

use app\components\CustomerJourneyService;
use app\components\HitService;
use app\models\LinkHit;
use yii\rest\ActiveController;

class HitController extends ActiveController
{
    public $modelClass = LinkHit::class;
    /**
     * @var HitService
     */
    public $service;

    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = new HitService();
    }

    public function actionLinkCount($link, $start, $end)
    {
        return $this->service->getLinkCount($link, (int)$start, (int)$end);
    }

    public function actionPageCount($start, $end)
    {
        // don't know if pagination should be done here
        return $this->service->getPageCount((int)$start, (int)$end);
    }

    public function actionCustomerJourney($customer_id)
    {
        // don't know if pagination should be done here
        $customerJourneyService = new CustomerJourneyService();

        $data['customer_journey'] = $customerJourneyService->findCustomerJourney($customer_id);
        $data['similar_customers'] = $this->service->getSimilarCustomers($customerJourneyService, $customer_id);

        return $data;
    }
}