<?php

namespace app\tests\unit\fixtures;

use app\models\LinkHit;
use yii\test\ActiveFixture;

class LinkHitFixture extends ActiveFixture
{
    public $modelClass = LinkHit::class;
}