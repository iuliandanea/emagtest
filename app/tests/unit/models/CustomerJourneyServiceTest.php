<?php

namespace tests\unit\models;

use app\components\CustomerJourneyService;
use PHPUnit\Framework\MockObject\MockObject;

class CustomerJourneyServiceTest extends \Codeception\Test\Unit
{

    public function testComputeCustomerJourneyHash()
    {
        /** @var MockObject|CustomerJourneyService $service */
        $service = $this->getMockBuilder(CustomerJourneyService::class)
            ->onlyMethods(['findCustomerJourney'])->getMock();
        $service->method('findCustomerJourney')->willReturn([
            ['url' => 'url1', 'hit_timestamp' => 12351213],
            ['url' => 'url2', 'hit_timestamp' => 12351213],
        ]);

        self::assertEquals('1b93df96393c24e4440a0ed16db80532', $service->computeCustomerJourneyHash(1));
    }
}
