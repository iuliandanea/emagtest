<?php

namespace app\components;

use app\models\LinkHit;
use yii\db\Query;

/**
 * Class HitService
 * @package app\components
 */
class HitService
{
    public function getLinkCount(string $link, int $start, int $end): int
    {
        $query = LinkHit::find()->where(['url' => $link])
            ->andWhere(['>=', 'hit_timestamp', $start])
            ->andWhere(['<=', 'hit_timestamp', $end]);
        return $query->count();
    }

    /**
     * @param int $start
     * @param int $end
     * @return array
     */
    public function getPageCount(int $start, int $end): array
    {
        $query = (new Query())
            ->select('count(*) as page_count, url')
            ->from('link_hit')
            ->where(['>=', 'hit_timestamp', $start])
            ->andWhere(['<=', 'hit_timestamp', $end])
            ->orderBy(['page_count' => SORT_DESC])
            ->groupBy(['url']);

        return $query->all();
    }

    /**
     * @param CustomerJourneyService $service
     * @param string $customer_id
     * @return array
     */
    public function getSimilarCustomers(CustomerJourneyService $service, string $customer_id): array
    {
        $journey = $service->computeCustomerJourneyHash($customer_id);
        $query = (new Query())
            ->select(['id'])
            ->from('customer')
            ->where(['journey' => $journey]);
        return $query->all();
    }
}