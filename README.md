# README #

Steps are necessary to get the application up and running.

### SETUP ###

```git clone git@bitbucket.org:iuliandanea/emagtest.git```

```cd emagtest/app```

```composer install```

```cd ../```

```docker-compose up -d```

Wait 30-60 seconds for mysql image to start

```docker-compose run -w /var/www/html/test webtest php yii migrate```

Example requests are provided in test.postman_collection.json

### Running Tests ###

Setup Test DB
```
docker-compose run -w /var/www/html/test webtest php tests/bin/yii migrate
```
Run Tests
```
docker-compose run -w /var/www/html/test webtest vendor/bin/codecept run unit
```
