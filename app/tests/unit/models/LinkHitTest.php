<?php

namespace tests\unit\models;

use app\models\Customer;
use app\models\LinkHit;
use app\tests\unit\fixtures\CustomerFixture;
use app\tests\unit\fixtures\LinkHitFixture;

class LinkHitTest extends \Codeception\Test\Unit
{

    public function _fixtures()
    {
        return [
            'link_hits' => [
                'class' => LinkHitFixture::class,
                'dataFile' => codecept_data_dir() . 'link_hit.php',
            ],
            'customers' => [
                'class' => CustomerFixture::class,
                'dataFile' => codecept_data_dir() . 'customer.php',
            ],
        ];
    }

    public function testLinkSaveUpdatesCustomerJourney()
    {
        /** @var Customer $customer */
        $customer = Customer::find()->where(['id' => 'c1'])->one();
        $hit = new LinkHit();
        $hit->url = 'url4';
        $hit->link_type = 'checkout';
        $hit->hit_timestamp = 12388;
        $hit->customer_id = $customer->id;

        self::assertEquals('1b93df96393c24e4440a0ed16db80532', $customer->journey);
        self::assertTrue($hit->save());

        // refresh and check if journey has been updated
        $customer->refresh();
        self::assertEquals('863fd62665edbac8fc0662f073a34e83', $customer->journey);
    }
}
