<?php

namespace tests\unit\models;

use app\components\CustomerJourneyService;
use app\components\HitService;
use app\tests\unit\fixtures\CustomerFixture;
use app\tests\unit\fixtures\LinkHitFixture;

class HitServiceTest extends \Codeception\Test\Unit
{

    public function _fixtures()
    {
        return [
            'link_hits' => [
                'class' => LinkHitFixture::class,
                'dataFile' => codecept_data_dir() . 'link_hit.php',
            ],
            'customers' => [
                'class' => CustomerFixture::class,
                'dataFile' => codecept_data_dir() . 'customer.php',
            ],
        ];
    }

    /**
     * @dataProvider linkCountTestData
     * @param string $url
     * @param int $start
     * @param int $end
     * @param int $expected_count
     */
    public function testLinkCount(string $url, int $start, int $end, int $expected_count)
    {
        $service = new HitService();
        $count = $service->getLinkCount($url, $start, $end);

        self::assertEquals($expected_count, $count);
    }

    public function testGetSimilarCustomers()
    {
        $hitService = new HitService();
        $journeyService = new CustomerJourneyService();

        $similarCustomers = $hitService->getSimilarCustomers($journeyService, 'c1');
        self::assertEquals([['id' => 'c1'], ['id' => 'c2']], $similarCustomers);
    }


    public function linkCountTestData()
    {
        return [
            ['url1', 12345, 12346, 2],
            ['url1', 12344, 12344, 0],
            ['url3', 12347, 12377, 1],
        ];
    }
}
