<?php

namespace app\components;

use yii\db\Query;
use yii\helpers\ArrayHelper;

class CustomerJourneyService
{
    /**
     * @param $customer_id
     * @return array
     */
    public function findCustomerJourney($customer_id): array
    {
        $query = (new Query())
            ->select(['url', 'hit_timestamp'])
            ->from('link_hit')
            ->where(['customer_id' => $customer_id])
            ->orderBy(['hit_timestamp' => SORT_ASC]);
        return $query->all();
    }

    /**
     * @param $customer_id
     * @return string
     */
    public function computeCustomerJourneyHash($customer_id): string
    {
        $journey = $this->findCustomerJourney($customer_id);
        $urls = implode(',', ArrayHelper::getColumn($journey, 'url'));
        return md5($urls);
    }
}