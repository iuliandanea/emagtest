<?php

return [
    'hit1' => [
        'id' => '1',
        'url' => 'url1',
        'link_type' => 'checkout',
        'hit_timestamp' => 12345,
        'customer_id' => 'c1',
    ],
    'hit2' => [
        'id' => '2',
        'url' => 'url2',
        'hit_timestamp' => 12346,
        'link_type' => 'checkout',
        'customer_id' => 'c1',
    ],
    'hit3' => [
        'id' => '3',
        'url' => 'url1',
        'link_type' => 'checkout',
        'hit_timestamp' => 12345,
        'customer_id' => 'c2',
    ],
    'hit4' => [
        'id' => '4',
        'url' => 'url2',
        'hit_timestamp' => 12346,
        'link_type' => 'checkout',
        'customer_id' => 'c2',
    ],
    'hit5' => [
        'id' => '5',
        'url' => 'url3',
        'hit_timestamp' => 12376,
        'link_type' => 'checkout',
        'customer_id' => 'c3',
    ],
];